DESCRIPTION:
------------
The Referral Agency module processes messages from Salesforce to generate
a list of Referral Agencies on a Drupal web page.
This is the customised module for The FRU.

For further Help see: 'Help >> Referral Agency'.

INSTALLATION:
-------------
1. Extract the tar.gz into your 'sites/all/modules/' directory.
2. Enable the module at 'Modules >> Referral Agency'.

CONFIGURATION
-------------
1. Visit 'Configuration >> Referral Agency settings'

UNINSTALLTION:
--------------
1. Disable the module.
2. Uninstall the module, which will drop all the Referral Agency data.

CREDITS:
--------
Written by Brian Green <brian.green@bdgreen.it>
Maintained by Brian Green

