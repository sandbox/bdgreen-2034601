<?php
/**
 * @file
 * List Referral Agency details from Salesforce (SFDC) records.
 */

 /**
 * Implements hook_menu().
 * Menu entries) for the referral_agency module.
 */
function referral_agency_menu() {
  // SFDC SOAP endpoint.
  $items['referral_agency_transactions'] = array(
    'title' => FALSE,
    'page callback' => 'referral_agency_transactions',
    'access arguments' => array('access content'),
    'access callback' => 'referral_agency_notifications_endpoint',
    'type' => MENU_CALLBACK,
  );
  // Admin menu for settings.
  $items['admin/config/content/referral_agency'] = array(
    'title' => 'Referral Agency settings',
    'description' => 'Modify layout of Referral Agencies.',
    'page callback' => 'drupal_get_form',
    'page arguments' => array('referral_agency_settings'),
    'access callback' => 'user_access',
    'access arguments' => array('administer referral_agency settings'),
    'type' => MENU_NORMAL_ITEM,
  );

  return $items;
}

/**
 * Implements hook_help.
 */
function referral_agency_help($path, $arg) {
  switch ($path) {
    case "admin/help#referral_agency":
      $output = '';
      $output .= '<h3>' . t('About') . '</h3>';
      $output .= '<p>'  . t('The Referral Agency module processes messages from Salesforce to generate a list of Referral Agencies on a Drupal web page. This is the customised module for <a href="@the_fru">The FRU</a>.  For more information, see the online handbook for the <a href="@referral_agency">Referral Agency module</a>.', array('@referral_agency' => 'http://insiteful.it/sites/all/documents/referral_agency.php', '@the_fru' => url('http://thefru.org.uk/'))) . '</p>';
      $output .= '<h3>' . t('Uses') . '</h3>';
      $output .= '<dl>';
      $output .= '<dt>' . t('Install') . '</dt>';
      $output .= '<dd>' . t('After installing, and enabling, the Referral Agency module appropriate Outbound message from Salesforce that have been triggered by a Workflow rule that monitors changes to fields of the Account records will update the Drupal database. Any changes to Referral Agency details in Salesforce will be immediately available for listing on Drupal web page.') . '</dd>';
      $output .= '<dt>' . t('In use') . '</dt>';
      $output .= '<dd>' . t('As a measure of security the Salesforce Organisation ID, that is the source of the Salesforce messages, must be entered into the <a href="@referral_agency">Settings page</a> of the Referral Agency module.', array('@referral_agency' => url('admin/config/content/referral_agency'))) . '</dd>';
      $output .= '<dt>' . t('Uninstall') . '</dt>';
      $output .= '<dd>' . t('Uninstalling the module will lose all the existing Referral Agency details in Drupal - Salesforce will remain unchanged.') . '</dd>';
      $output .= '<dt>' . t('Misc') . '</dt>';
      $output .= '<dd>' . t('One method that can be used to transfer all the current Referral Agency details to Drupal is to run an <em>Upsert</em>, against Salesforce, of the Salesforce record ID and the Referral Agency Name of all the existing Referral Agencies details in Salesforce.  This will generate, or re-generate, a full listing of all Referral Agency details in Drupal.  The Salesforce <a href="@data_loader">Data Loader</a> can be used for this <em>Export</em> and <em>Upsert</em> task.  OBSERVE: Referral Agency records, in Salesforce, should be first <em>Unpublished</em> before being <em>deleted</em>!', array('@data_loader' => url('http://wiki.developerforce.com/page/Data_Loader'))) . '</dd>';
      $output .= '</dl>';
      return $output;
      break;
  }
}

/**
 * Menu callback for SFDC notifications endpoint
 */
function referral_agency_notifications_endpoint() {
  // Set up variables
  $audit      = (variable_get('referral_agency_audit') == 2);
  $audit_file =  variable_get('referral_agency_audit_file');
  //
  $content = file_get_contents('php://input');
  if (empty($content)) {
    watchdog('Referral Agency module', 'Salesforce Notifications: Empty request.', NULL, $severity = WATCHDOG_DEBUG, $link = NULL);
    exit;
  }
  
  if ($audit) {
    $fh = fopen($audit_file, 'w');
    fwrite($fh, 'Recieved SOAP @ ' . date('l jS \of F Y h:i:s A') . "\n");
    fwrite($fh, str_repeat("=", 60) . "\n");
    $ret = fclose($fh);
  }
    
  $dom = new DOMDocument();
  $dom->loadXML($content);

  if ($audit) {
    $fh = fopen($audit_file, 'a');
    fwrite($fh,'Dom:' . "\n" . $dom->saveXML() . "\n");
    fwrite($fh, str_repeat("=", 60) . "\n");
    $ret = fclose($fh);
  }
  
  $resultArray = referral_agency_notifications_parse_message($dom);

  if ($audit) {
    $fh = fopen($audit_file, 'a');
    fwrite($fh, 'Array:' . "\n" . print_r($resultArray, TRUE) . "\n");
    fwrite($fh, str_repeat("=", 60) . "\n");
    $ret = fclose($fh);
  }
  if ($resultArray != NULL) {
    $ret = referral_agency_notifications_handle_message($resultArray);
  } else {
    $ret = TRUE;
  }
  // Sends SOAP response to SFDC
  if ($ret) {
    referral_agency_notifications_respond('true');
  }
  else {
    referral_agency_notifications_respond('false');
  }
  exit;
}

/**
 * Parse Referral Agency Dom Document to Referral Agency array.
 */
function referral_agency_notifications_parse_message($domDoc) {
  $i = 0;  // Counter for Outbound messages of multiple Notifications
  $ra = Array("Account" => array(0 => array("Id" => "",
                                   "BillingCity" => "",
                             "BillingPostalCode" => "",
                                  "BillingState" => "",
                                 "BillingStreet" => "",
                         "Details_To_Publish__c" => "",
                                      "Email__c" => "",
                                          "Name" => "",
                                         "Phone" => "",
                         "Publish_on_Website__c" => "",
                                  "RecordTypeId" => "",
                     "Referral_Agency_Status__c" => "",
                                       "Website" => "",
                                           ),
                                ),
              ); 
  $OrganizationId = $domDoc->getElementsByTagName("OrganizationId")->item(0)->textContent;
  //
  // Basic Organisation validation.
  //
  If (substr($OrganizationId, 0, 15) !== variable_get('referral_agency_organizationid')) {
    watchdog('Referral Agency module', 'Invalid source organisation!', NULL, $severity = WATCHDOG_CRITICAL, $link = NULL);
    $ra = NULL;
  } else {
    watchdog('Referral Agency module', 'SOAP message recieved.', NULL, $severity = WATCHDOG_INFO, $link = NULL);
    $objects = $domDoc->getElementsByTagName('sObject');
    foreach ($objects as $sObjectNode) {
      $sObjType = $sObjectNode->getAttribute('xsi:type');
      if (substr_count($sObjType, 'sf:')) {
        $sObjType = substr($sObjType, 3);  // This should be "Account"!
      }
      $elements = $sObjectNode->getElementsByTagNameNS('urn:sobject.enterprise.soap.sforce.com', '*');
      foreach ($elements as $node) {
        // NB Notification "Id" available as SObject->fields i.e. $node->textContent;
        $ra[$sObjType][$i][$node->localName] =  $node->nodeValue;
      }
      $i++;
    }
  }
return $ra;
}

/**
 * Process Referral Agency array.
 */
function referral_agency_notifications_handle_message($resultArray) {
  foreach ($resultArray as $key=>$value) {
    switch ($key) {
      case "Account":
        $ret = referral_agency_notifications_handle_upsert($value);
        break;
    }
  }
  return $ret;
}

/**
 * Upsert (Insert or Update) Referral Agency records into local table.
 * The SFDC Id field is treated as the Unique (external) key for the Upsert.
 **/
function referral_agency_notifications_handle_upsert($records) {
  foreach ($records as $key=>$item) {
    $id  = $item['Id'];
    $result = db_select('referral_agency', 'ra')
            ->fields('ra')
            ->condition('ra.Id', $id)
            ->execute();
    $ids_found = $result->rowCount();
    if ($ids_found == 0) {
      //  New record so Insert it.
      watchdog('Referral Agency module', 'Doing Insert with ID="' . $id . '"', NULL, $severity = WATCHDOG_INFO, $link = NULL);
      $ret = db_insert('referral_agency')
           ->fields(array(
                                'Id' => $item['Id'],
                       'BillingCity' => $item['BillingCity'] . "",          // Null field fix
                 'BillingPostalCode' => $item['BillingPostalCode'] . "",    // Null field fix
                      'BillingState' => $item['BillingState'] . "",         // Null field fix
                     'BillingStreet' => $item['BillingStreet'] . "",        // Null field fix
                'Details_To_Publish' => $item['Details_To_Publish__c'],
                             'Email' => $item['Email__c'] . "",             // Null field fix
                              'Name' => $item['Name'],
                             'Phone' => $item['Phone'] . "",                // Null field fix
                'Publish_on_Website' => $item['Publish_on_Website__c'],
                      'RecordTypeId' => $item['RecordTypeId'],
            'Referral_Agency_Status' => $item['Referral_Agency_Status__c'],
                           'Website' => $item['Website'] . "",              // Null field fix
                          )
                   )
           ->execute();
    } else {
      // A record already exists so Update it.
      watchdog('Referral Agency module', 'Doing Update with ID="' . $id . '"', NULL, $severity = WATCHDOG_INFO, $link = NULL);
      $ret = db_update('referral_agency')
           ->fields(array(
                                'Id' => $item['Id'],
                       'BillingCity' => $item['BillingCity'] . "",          // Null field fix
                 'BillingPostalCode' => $item['BillingPostalCode'] . "",    // Null field fix
                      'BillingState' => $item['BillingState'] . "",         // Null field fix
                     'BillingStreet' => $item['BillingStreet'] . "",        // Null field fix
                'Details_To_Publish' => $item['Details_To_Publish__c'],
                             'Email' => $item['Email__c'] . "",             // Null field fix
                              'Name' => $item['Name'],
                             'Phone' => $item['Phone'] . "",                // Null field fix
                'Publish_on_Website' => $item['Publish_on_Website__c'],
                      'RecordTypeId' => $item['RecordTypeId'],
            'Referral_Agency_Status' => $item['Referral_Agency_Status__c'],
                           'Website' => $item['Website'] . "",              // Null field fix
                          )
                   )
           ->condition('Id', $id)
           ->execute();
    }
  }
return TRUE;
}


/**
 * Referral Agency administration settings.
 */
function referral_agency_settings() {
  // Organisation ID
  $form['referral_agency_organizationid'] = array(
    '#type' => 'textfield',
    '#size' => 15,
    '#maxlength' => 15,
    '#required' => TRUE,
    '#title' => t('Organisation ID.'),
    '#description' => t('Enter Salesforce Organisation ID.'),
    '#default_value' => variable_get('referral_agency_organizationid', ""),
  );
  // Display box width.
  $form['referral_agency_width'] = array(
    '#type' => 'textfield',
    '#size' => 4,
    '#maxlength' => 4,
    '#required' => TRUE,
    '#title' => t('Customise width of display box.'),
    '#description' => t('Enter an integer for the width of the display box. The default value is 800 (i.e. 800px).'),
    '#default_value' => variable_get('referral_agency_width', '600'),
  );
  // Display box height.
  $form['referral_agency_height'] = array(
    '#type' => 'textfield',
    '#size' => 4,
    '#maxlength' => 4,
    '#required' => TRUE,
    '#title' => t('Customise height of display box.'),
    '#description' => t('Enter an integer for the height of the display box. The default value is is 1000 (i.e. 1000px).'),
    '#default_value' => variable_get('referral_agency_height', '1000'),
  );
  // Heading/Name Font size.
  $form['referral_agency_fontsize'] = array(
    '#type' => 'textfield',
    '#size' => 1,
    '#maxlength' => 1,
    '#required' => TRUE,
    '#title' => t('Customise Name font size.'),
    '#description' => t('Modify the Name font size, an integer between 1 and 5. Default size is "3".'),
    '#default_value' => variable_get('referral_agency_fontsize', '3'),
  );
  // Display box border.
  $form['referral_agency_border'] = array(
    '#type' => 'textfield',
    '#size' => 40,
    '#maxlength' => 40,
    '#required' => TRUE,
    '#title' => t('Customise display box colour.'),
    '#description' => t('Modify with care! Default is "border:1px solid #ccc;"'),
    '#default_value' => variable_get('referral_agency_border', 'border:1px solid #ccc;'),
  );
  // Audit Log
  $form['referral_agency_audit'] = array(
    '#type' => 'select',
    '#title' => t('Write to Outbound message audit file.'),
    '#default_value' => variable_get('referral_agency_audit', 1),
    '#options' => array(
      1 => 'No audit file.',
      2 => 'Full audit file.',
    ),
    '#description' => t('Write to Outbound message audit file. Choose an option - USE with care!'),
  );
  // Display box border.
  $form['referral_agency_audit_file'] = array(
    '#type' => 'textfield',
    '#size' => 80,
    '#maxlength' => 80,
    '#required' => TRUE,
    '#title' => t('Outbound message audit full file name.'),
    '#description' => t('Modify with care! Default value is "sites/all/modules/referral_agency/SOAP_Audit.txt"'),
    '#default_value' => variable_get('referral_agency_audit_file', 'sites/all/modules/referral_agency/SOAP_Audit.txt'),
  );
   
  return system_settings_form($form);
}

/**
 * Validate functions for Settings.
 */
function referral_agency_settings_validate($form, &$form_state) {
  // Validate display width.
  $width = $form_state['values']['referral_agency_width'];
  if ($width && ($width < 100 || $width > 800)) {
    form_set_error('referral_agency_width', t('Width should be between 100 and 800.'));
  }
  // Validate display height.
  $height = $form_state['values']['referral_agency_height'];
  if ($height && ($height < 200 || $height > 1200)) {
    form_set_error('referral_agency_height', t('Width should be between 200 and 1200.'));
  }
  // Validate display Name size.
  $fontsize = $form_state['values']['referral_agency_fontsize'];
  if ($fontsize && ($fontsize < 1 || $fontsize > 5)) {
    form_set_error('referral_agency_fontsize', t('Font size should be between 1 and 5.'));
  }
}

/**
 * Implement Display function
 */
function referral_agency_listing() {
  // Constants.
  $prefix  = '<h'  . variable_get('referral_agency_fontsize') . ' class=referral_agency_heading >';
  $postfix = '</h' . variable_get('referral_agency_fontsize') . '>';
  $break   = '<br />';

  // Header CSS for display.
  $display  = '<div class=referral_agency style="height:' . variable_get('referral_agency_height');
  $display .= 'px;width:' . variable_get('referral_agency_width');
  $display .= 'px;' . variable_get('referral_agency_border') . 'overflow:auto;">';
  print $display;

  // Pull listing from Referral Agency table.
  $result = db_select('referral_agency', 'ra')
    ->fields('ra')
    ->condition('Publish_on_Website', 'true', '=')
    ->condition('Referral_Agency_Status', 'Current', '=')
    ->orderBy('Name')
    ->execute();
  // Null result?  
  if ($result->rowCount() > 0) {  
    foreach ($result as $record) {
      // For each row.
      // Publish this field?
      $publish = $record->Details_To_Publish;
      // Display Name.
      $display  = $prefix . $record->Name . $postfix;
      $display .= referral_agency_print($publish, "Address", "",          $record, $break);
      $display .= referral_agency_print($publish, "Phone",   "Tel. ",     $record, $break);
      $display .= referral_agency_print($publish, "Email",   "Email: ",   $record, $break);
      $display .= referral_agency_print($publish, "Website", "Website: ", $record, $break);
      // Display rest of record.
      print $display;
    }
  } else {
      print "D'oh! No records yet - have you configured Salesforce?" . $break;
    }
  // Final </div>.
  print '</div>';
  return;
}

/**
 * Prepare non-null fields to display.
 */
function referral_agency_print($publish, $type, $label, $record, $break) {
  $line = "";
  if (strrpos($publish, $type) !== false) {
    switch ($type) {
      case "Address":
        $line =  if_not_null($label, $record->BillingStreet, $break);
        $line .= if_not_null($label, $record->BillingCity,   $break);
        $line .= if_not_null($label, $record->BillingState,  $break);
        $line .= $not_null = if_not_null($label, $record->BillingPostalCode, $break);
        if ($not_null) {
          $line .= "<a href='https://maps.google.co.uk/maps?q=$record->BillingPostalCode' target='_blank'>Map</a>" . $break;
        }
        break;
      case "Phone":
         $field = $record->Phone;
         if ($field != "") {
           $line .= $label . $field . $break;
         }
        break;
      case "Email":
         $field = $record->Email;
         if ($field != "") {
           $line .= $label . '<a href="mailto:' . $field . '">' . $field . '</a>' . $break;
         }
         break;
      case "Website":
         $field = $record->Website;
         if ($field != "") {
           $line = $label . '<a href="http://' . $field . '" title="' . $field . '">' . $field . '</a>' . $break;
         }
         break;
    }
  }
  return $line;
}

/**
 * Ensure that only none null fields are displayed.
 */
function if_not_null($prefix, $field, $break) {
  $ret = NULL;
  if ($field != "") {
    $ret = $prefix . $field . $break;
    }
  return $ret;
}

/**
 * Implement SOAP Response to Outbound message
 */
function referral_agency_notifications_respond($tf = 'true') {
  print '<?xml version = "1.0" encoding = "utf-8"?>
<soapenv:Envelope xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/"
  xmlns:xsd="http://www.w3.org/2001/XMLSchema"
  xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
     <soapenv:Body>
        <notifications xmlns="http://soap.sforce.com/2005/09/outbound">
           <Ack>' . $tf . '</Ack>
        </notifications>
     </soapenv:Body>
</soapenv:Envelope>';
}
